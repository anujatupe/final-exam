
#include <stdlib.h>
#include "mpi.h"
#include <stdio.h>
#include <math.h>

int curr_rank, world_size, mpi_err;

/*Generate random elements for the array
 */
void generate_elements(double *local_arr, int elements_per_process){
    int i;
    for (i = 0; i < elements_per_process; i++) {
        //double curr_ele = (double)(rand() % 10);
        double curr_ele = (double)(i);
        local_arr[i] = curr_ele;
        //printf("%lf+", curr_ele);
    }
}

/*Find sum of all elements in the array 
 */
double find_total(double *local_arr, int elements_per_process) {
    int i;
    double total = 0.0;
    for (i = 0; i < elements_per_process; i++) {
        total += local_arr[i];
    }
    return total;
}

/* Find sum of values obtained after squaring subtraction of element and mean value
 */
double find_sq_total(double *local_arr, double mean_val, int elements_per_process) {
    int i;
    double total = 0.0;
    for (i = 0; i < elements_per_process; i++) {
        total += (local_arr[i] - mean_val) * (local_arr[i] - mean_val);
    }
    return total;
}

/* Initialize the mpi world
 * Get number of processes
 * Get rank of every process
 */
void initialize_mpi_world() {
    mpi_err = MPI_Init(NULL, NULL);
    mpi_err = MPI_Comm_size( MPI_COMM_WORLD, &world_size);
    mpi_err = MPI_Comm_rank(MPI_COMM_WORLD, &curr_rank);
}

int main(int argc, char *argv[] )
{
    double start_time, end_time;
    int  n, total_eles;
    double final_variance_val, local_sum,  local_sq_sum;
    double *local_arr;
    
    int eles_per_process;

    //Get number of elements per process
    eles_per_process = atoi(argv[1]);
    
    initialize_mpi_world();

    if(curr_rank == 0) {
        total_eles = eles_per_process * world_size;
    }
    int i;

    //Start time of program
    start_time = MPI_Wtime();

    //Generating elements
    local_arr = (double *)malloc( eles_per_process * sizeof(double));

    generate_elements(local_arr, eles_per_process);

    // Finding the total sum for mean
    double l_total_sum = 0.0;
    l_total_sum = find_total(local_arr, eles_per_process);


    //Reducing the local sums to one global sum
    double global_sum;
    MPI_Allreduce(&l_total_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    // Finding thr mean of the elements in the array
    double final_mean_val = global_sum/(double)(eles_per_process * world_size);

    // Finding the sum of numbers after subtracting the mean anf squaring the result
    double l_sq_total_sum = 0.0;
    l_sq_total_sum = find_sq_total(local_arr, final_mean_val, eles_per_process);

    //Reducing the local sq sums to one global sq sum
    double global_sq_sum;
    MPI_Reduce(&l_sq_total_sum, &global_sq_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    //End time of program
    end_time = MPI_Wtime();
 
    if(curr_rank == 0) {
        final_variance_val = global_sq_sum/(double)(total_eles - 1);
        printf("\nThe Mean value of the array is %lf\n", final_mean_val);
        printf("\nThe Variance of the array is %lf\n", final_variance_val);
        float elapsed_time = end_time - start_time;
        printf("\nTime required to execute the program is %f seconds\n\n", elapsed_time);
    }
 
    MPI_Finalize();
    return 0;
}