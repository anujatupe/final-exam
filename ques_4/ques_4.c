
#include <stdlib.h>
#include "mpi.h"
#include <stdio.h>
#include <math.h>

int curr_rank, world_size, mpi_err;

void initialize_mpi_world() {
    mpi_err = MPI_Init(NULL, NULL);
    mpi_err = MPI_Comm_size( MPI_COMM_WORLD, &world_size);
    mpi_err = MPI_Comm_rank(MPI_COMM_WORLD, &curr_rank);
}

int main(int argc, char *argv[] )
{
    double start_time, end_time;
    int i, n, total_eles;
    double final_mean_val, final_variance_val, local_sum, global_sum, local_sq_sum, global_sq_sum;

    total_eles = atoi(argv[1]);
    
    initialize_mpi_world();

    if (curr_rank == 0) {
        n = world_size * total_eles;
    }
 
    start_time = MPI_Wtime();

    local_sum = local_sq_sum = global_sum = global_sq_sum = 0;
    
    for(i = 0; i < total_eles; i++) {
        local_sum  += (double)i;
        local_sq_sum += (double)i * (double)i;
    }
 
    //printf("\nMyid: %d %f\n", curr_rank, local_sum);
    MPI_Reduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&local_sq_sum, &global_sq_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    end_time = MPI_Wtime();
 
    if(curr_rank == 0) {
        final_mean_val = global_sum/((double)(n));
        final_variance_val = (global_sq_sum - global_sum * global_sum/n)/n;
        printf("\nThe values in the array are consecutive numbers starting 0 to total elements given as input to the program\n");
        printf("\nThe Mean value of the array is %lf\n", final_mean_val);
        printf("\nThe Variance of the array is %lf\n", final_variance_val);
        float elapsed_time = end_time - start_time;
        printf("\nTime required to execute the program is %f seconds\n\n", elapsed_time);
    }
 
    MPI_Finalize();
    return 0;
}