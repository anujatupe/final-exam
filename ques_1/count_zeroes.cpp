#include <iostream>
#include <time.h>
#include <omp.h>
#include <cstdint>

#define SECINNANOSEC 1000000000L

using namespace std;

int main() {
  int no, i, total_number_of_zeroes;
  int *arr;
  timespec start_time, end_time;
  uint64_t diff_time;
  
  cout<<"\nEnter the number of elements in the array:";
  cin>>no;
  
  arr = (int *) malloc(no * sizeof(int));
  
  cout<<"\nGenerating random array of "<<no<<" numbers";
  for(i = 0; i < no; i++){
  	arr[i] = rand() % 10;
  }

  //cout<<"\nGenerated array is: ";
  // for(i = 0; i < no; i++){
  //        cout<<arr[i]<<" ";
  //    }

  total_number_of_zeroes = 0;
  
  clock_gettime(CLOCK_MONOTONIC, &start_time);
  #pragma omp parallel for reduction(+:total_number_of_zeroes) schedule(static)
  for (i = 0; i < no; i++) {
    if (arr[i] == 0) {
      total_number_of_zeroes += 1;
    }
  }
  clock_gettime(CLOCK_MONOTONIC, &end_time);
  
  cout<<"\n****Total zeroes are "<<total_number_of_zeroes<<" ****\n";
  
  diff_time = SECINNANOSEC * (end_time.tv_sec - start_time.tv_sec) + end_time.tv_nsec - start_time.tv_nsec;
  cout<<"\n\n***PARALLEL VERSION - Time required for counting zeroes: "<<(long long unsigned int) diff_time<<" nanoseconds ***\n\n";
  
  free(arr);  
}