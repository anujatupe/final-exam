#include<iostream>
#include<time.h>
#include<math.h>
#include<cstdlib>
#include<algorithm>

using namespace std;

double get_mean_value(double *arr, int count) {
  double total_sum = 0;
  int i;
  for(i = 0; i < count; i++){
    total_sum += arr[i];
  }
  return (total_sum/(double)count);
}

double get_variance_value(double *arr, int count) {
  double mean_val = get_mean_value(arr, count);
  double sq_sum = 0;

  int i;
  for(i = 0; i < count; i++){
    double ele_val = (arr[i] - mean_val) * (arr[i] - mean_val);
    sq_sum += ele_val;
  }
  return (sq_sum/(double)count);
}



double get_median_value(double *arr, int count) {
  sort(arr, arr + count);
  int i = 0;
  //cout<<"\nSORTED ARRAY"<<endl;
  //for(i = 0; i < count; ++i) {
  //  printf("%lf ", arr[i]);
  //}
  int middle_index = count/2;
  if (count % 2 != 0) {
    return arr[middle_index];
  } else {
    return (arr[middle_index - 1] + arr[middle_index])/2.0;
  }
  return 0.0;
}

double* get_random_array(int count) {
  double *arr = (double *) malloc(count * sizeof(double));
  int i = 0;
  for(i = 0; i < count; ++i) {
    arr[i]= (double)(rand() % 10);
    //arr[i]= (double)i;
    //printf("%lf ", arr[i]);
  }

  return arr;
}


int main(int argc, char **argv) {

  unsigned long count = atoi(argv[1]);

  double *arr = get_random_array(count);

  double seq_mean = get_mean_value(arr, count);
  double seq_var = get_variance_value(arr, count);
  double seq_median = get_median_value(arr, count);

  cout << "\nSequential Mean is: " << seq_mean << endl;
  cout << "\nSequential Variance is: " << seq_var << endl;
  cout << "\nSequential Median is: " << seq_median << endl;
  return 0;
}
