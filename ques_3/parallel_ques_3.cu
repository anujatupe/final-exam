#include<math.h>
#include<time.h>
#include<iostream>
#include <thrust/sort.h>

#define BLOCK_SIZE 512

using namespace std;

float global_elapsed_time_mean = 0.0;
float global_elapsed_time_variance = 0.0;
float global_elapsed_time_median = 0.0;

double* get_random_array(unsigned long total_eles) {
  double *arr = (double *) malloc(total_eles * sizeof(double));
  unsigned long i = 0;
  for(i = 0; i < total_eles; ++i) {
    arr[i]= (double)(rand() % 10);
    //arr[i]= (double)i;
  }
  return arr;
}

__global__ void get_sq_array(double *list, double *output_list, double mean_val, unsigned long total_eles) {

  unsigned int global_id = blockDim.x * blockIdx.x + threadIdx.x;

  if (global_id < total_eles) {
    double ele_val = list[global_id] - mean_val;
    output_list[global_id] = ele_val * ele_val;
  }

  __syncthreads();
}

double* get_squared_array(double *h_arr, double mean_val, unsigned long total_eles) {

  cudaError_t err;
  int no_of_blocks;
  double *d_in_arr;
  double *d_out_arr;
  cudaEvent_t start, stop;
  float elapsed_time;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  no_of_blocks = ceil((double)total_eles/(double)BLOCK_SIZE);

  err = cudaMalloc(&d_in_arr, total_eles * sizeof(double));
  printf("\nCUDA Malloc input array: %s",cudaGetErrorString(err));
  err = cudaMemcpy(d_in_arr, h_arr, total_eles * sizeof(double), cudaMemcpyHostToDevice);
  printf("\nHost to device copy input array: %s",cudaGetErrorString(err));
  err = cudaMalloc(&d_out_arr, total_eles * sizeof(double));
  printf("\nCUDA Malloc output array: %s",cudaGetErrorString(err));
  err = cudaMemcpy(d_out_arr, h_arr, total_eles * sizeof(double), cudaMemcpyHostToDevice);
  printf("\nHost to device copy output array: %s",cudaGetErrorString(err));

  cudaEventRecord(start, 0);
  get_sq_array<<<no_of_blocks, BLOCK_SIZE>>>(d_in_arr, d_out_arr, mean_val, total_eles);
  cudaEventRecord(stop, 0);

  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&elapsed_time, start, stop);
  global_elapsed_time_variance = elapsed_time;

  err = cudaThreadSynchronize();
  printf("\nKernel SQUARE ARRAY ELEMENTS Function Execution: %s", cudaGetErrorString(err));

  cudaMemcpy(h_arr, d_out_arr, total_eles * sizeof(double), cudaMemcpyDeviceToHost);
  printf("\nDevice to host copy output array: %s\n",cudaGetErrorString(err));

  return h_arr;
}

__global__ void summation_reduction(double *d_in_arr, double *d_out_arr) {
  __shared__ double shared_arr[BLOCK_SIZE];

  unsigned int global_id = blockDim.x * blockIdx.x + threadIdx.x;
  unsigned int thread_id = threadIdx.x;

  shared_arr[thread_id] = d_in_arr[global_id];
  __syncthreads();

  unsigned long i;
  for(i = 1; i < blockDim.x; i *= 2) {
    if(thread_id % (i * 2) == 0) {
      shared_arr[thread_id] += shared_arr[thread_id + i];
      
    }
    __syncthreads();
  }

  if(thread_id == 0) {
    d_out_arr[blockIdx.x] = shared_arr[0];
  }
  __syncthreads();
}

double parallel_total_sum(double* h_arr, unsigned long total_eles) {

  unsigned long i = 0;
  int no_of_blocks;
  double *h_long_arr;
  double *h_long_arr2;
  double *d_in_arr;
  double *d_out_arr;
  cudaError_t err;
  cudaEvent_t start, stop;
  float elapsed_time;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  no_of_blocks = ceil((double)total_eles/(double)BLOCK_SIZE);

  h_long_arr = (double *) malloc(no_of_blocks * BLOCK_SIZE * sizeof(double));
  h_long_arr2 = (double *) calloc((no_of_blocks + BLOCK_SIZE), sizeof(double));

  for(i = 0; i < no_of_blocks * BLOCK_SIZE; i++){
    if (i < total_eles){
      h_long_arr[i] = h_arr[i];
    } else {
      h_long_arr[i] = 0.0;
    }
   }

  err = cudaMalloc(&d_in_arr, no_of_blocks * BLOCK_SIZE * sizeof(double));
  printf("\nCUDA Malloc input array: %s",cudaGetErrorString(err));
  err = cudaMemcpy(d_in_arr, h_long_arr, no_of_blocks * BLOCK_SIZE * sizeof(double), cudaMemcpyHostToDevice);
  printf("\nHost to device copy input array: %s",cudaGetErrorString(err));
  err = cudaMalloc(&d_out_arr, (no_of_blocks + BLOCK_SIZE) * sizeof(double));
  printf("\nCUDA Malloc output array: %s",cudaGetErrorString(err));
  err = cudaMemcpy(d_out_arr, h_long_arr2, (no_of_blocks + BLOCK_SIZE) * sizeof(double), cudaMemcpyHostToDevice);
  printf("\nHost to device copy output array: %s",cudaGetErrorString(err));

  cudaEventRecord(start, 0);

  while(no_of_blocks >= 1) {
    summation_reduction<<<no_of_blocks, BLOCK_SIZE>>>(d_in_arr, d_out_arr);
    if (no_of_blocks > 1) {
      err = cudaMemcpy(d_in_arr, d_out_arr, (no_of_blocks + BLOCK_SIZE) * sizeof(double), cudaMemcpyHostToDevice);
      err = cudaMemcpy(d_out_arr, h_long_arr2, (no_of_blocks + BLOCK_SIZE) * sizeof(double), cudaMemcpyHostToDevice);
      no_of_blocks = ceil((double) no_of_blocks / (double) BLOCK_SIZE);
    } else {
      no_of_blocks = 0;
    }
  }

  cudaEventRecord(stop, 0);

  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&elapsed_time, start, stop);
  global_elapsed_time_mean = elapsed_time;

  err = cudaThreadSynchronize();
  printf("\nKernel SUM Function Execution: %s", cudaGetErrorString(err));

  err = cudaMemcpy(h_long_arr, d_out_arr, sizeof(double), cudaMemcpyDeviceToHost);
  //err = cudaMemcpy(h_long_arr, d_in_arr, sizeof(double), cudaMemcpyDeviceToHost);
  printf("\nDevice to host copy output array: %s\n",cudaGetErrorString(err));
  return h_long_arr[0];
}

double parallel_median(double *h_arr, unsigned long total_eles) {
  
  //unsigned long i;
  double median_val;
  unsigned long middle_index;
  cudaEvent_t start, stop;
  cudaError_t err;
  float elapsed_time;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  // cout<<"\nUNSORTED ARRAY: \n";
  // for(i = 0; i < total_eles; i++) {
  //   cout<<h_arr[i]<<" ";
  // }
  cudaEventRecord(start, 0);
  thrust::sort(h_arr, h_arr + total_eles);

  // cout<<"\nSORTED ARRAY: \n";
  // for(i = 0; i < total_eles; i++) {
  //   cout<<h_arr[i]<<" ";
  // }

  middle_index = total_eles/2;

  if (total_eles % 2 != 0 ) {
    median_val = h_arr[middle_index];
  } else {
    median_val = (h_arr[middle_index - 1] + h_arr[middle_index])/2.0;
  }
  cudaEventRecord(stop, 0);

  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&elapsed_time, start, stop);
  global_elapsed_time_median = elapsed_time;

  err = cudaThreadSynchronize();
  printf("\nKernel MEDIAN Function Execution: %s", cudaGetErrorString(err));

  return median_val;
}


int main(int argc, char **argv) {
  unsigned long total_eles = atoi(argv[1]);

  double *arr = get_random_array(total_eles);

  double median_val = parallel_median(arr, total_eles);
  double simple_sum = parallel_total_sum(arr, total_eles);
  double mean_val = simple_sum/(double)total_eles;
  double *sq_arr = get_squared_array(arr, mean_val, total_eles);

  double sq_sum = parallel_total_sum(sq_arr, total_eles);
  double variance_val = sq_sum/(double)total_eles;
  
  cout << "\n***********Parallel mean is: " << mean_val << "***********" << endl;
  cout << "\n***********Parallel variance is: " << variance_val << "***********" << endl;
  cout << "\n***********Parallel median is: " << median_val << "***********" << endl;
  cout << "\nPARALLEL VERSION - Time required by mean is: "<< global_elapsed_time_mean << "ms\n" ;
  cout << "\nPARALLEL VERSION - Time required by variance is: "<< (global_elapsed_time_variance+global_elapsed_time_mean) << "ms\n" ;
  cout << "\nPARALLEL VERSION - Time required by median is: "<< global_elapsed_time_median << "ms\n";
  return 0;
}
