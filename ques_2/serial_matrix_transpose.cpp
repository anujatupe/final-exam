#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <time.h>

#define SECINNANOSEC 1000000000L

using namespace std;

void print_matrices(int *m1, int rows, int cols) {
  printf("\n");
  for(int i = 0; i < rows; i++) {
    printf("\n");
    for(int j = 0; j < cols; j++) {
      printf("%d\t", m1[i*cols + j]);
    }
  }
  printf("\n");
}

void serial_get_transpose(int *input_matrix, int *output_matrix, int rows, int cols) {
  for (int j = 0; j < rows; j++){
  	for (int i = 0; i < cols; i++){
  	  output_matrix[j + i*rows] = input_matrix[i + j*cols]; 
	}
  }
}

int main(int argc, char **argv) {
  timespec start_time, end_time;
  uint64_t diff_time;

  clock_gettime(CLOCK_MONOTONIC, &start_time);
  int rows = atoi(argv[1]);
  int cols = atoi(argv[2]);

  int *input_matrix = (int*)malloc(cols * rows * sizeof(int));
  int *output_matrix = (int*)malloc(cols * rows * sizeof(int));

  printf("\nGenerating Inpur Matrix...");

  int count = 1;
  for(int i = 0; i < rows; i++) {
    for(int j = 0; j < cols; j++) {
      input_matrix[i*cols + j] = count;
      count++;
    }
  }

  // printf("\nINPUT MATRIX:\n");
  // print_matrices(input_matrix, rows, cols);

  printf("\nFinding Transpose...");

  serial_get_transpose(input_matrix, output_matrix, rows, cols);
  
  // printf("\nTRANSPOSE MATRIX:\n");
  // print_matrices(output_matrix, cols, rows);

  clock_gettime(CLOCK_MONOTONIC, &end_time);

  diff_time = SECINNANOSEC * (end_time.tv_sec - start_time.tv_sec) + end_time.tv_nsec - start_time.tv_nsec;
  cout<<"\n\n***SERIAL VERSION - Time required to find matrix transpose is: "<<(long long unsigned int) diff_time<<" nanoseconds ***\n\n";

  free(input_matrix);
  free(output_matrix);

  return 0;
}