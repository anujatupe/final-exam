#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TILE_SIZE 32
#define BLOCK_SIZE 32

// void serial_get_transpose(int *input_matrix, int *output_matrix, int rows, int cols) {
//   for (int j = 0; j < rows; j++){
//   	for (int i = 0; i < cols; i++){
//   	  output_matrix[j + i*rows] = input_matrix[i + j*cols]; 
// 	}
//   }
// }


__global__ void get_transpose(int *d_in_matrix, int *d_out_matrix, int rows, int cols) {
  int i = blockIdx.x * TILE_SIZE + threadIdx.x;
  int j = blockIdx.y * TILE_SIZE + threadIdx.y;

  // int global_thread_x = (blockIdx.x * blockDim.x) + threadIdx.x;
  // int global_thread_y = (blockIdx.y * blockDim.y) + threadIdx.y;

  //if (((global_thread_y < (rows)) && (global_thread_y >= 0) && ((global_thread_x < (cols)) && (global_thread_x >= 0)))) { 
  if (((j < (rows)) && (j >= 0) && ((i < (cols)) && (i >= 0)))) { 
  	d_out_matrix[j + i*rows] = d_in_matrix[i + j*cols]; 
  }
}

void print_matrices(int *m1, int rows, int cols) {
  printf("\n");
  for(int i = 0; i < rows; i++) {
    printf("\n");
    for(int j = 0; j < cols; j++) {
      printf("%d\t", m1[i*cols + j]);
    }
  }
  printf("\n");
}


int main(int argc, char* argv[]) {
  int *d_in_matrix;
  int *d_out_matrix;
  size_t size_of_matrix;
  cudaError_t err;
  cudaEvent_t start, stop;
  float elapsed_time;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  int rows = atoi(argv[1]);
  int cols = atoi(argv[2]);
  //int mode = atoi(argv[3]);

  int *h_in_matrix = (int*)malloc(cols * rows * sizeof(int));
  int *h_out_matrix = (int*)malloc(cols * rows * sizeof(int));
  int *empty_matrix = (int*)malloc(cols * rows * sizeof(int));

  int count = 1;
  printf("\nGenerating Input Matrix..");
  for(int i = 0; i < rows; i++) {
    for(int j = 0; j < cols; j++) {
      h_in_matrix[i*cols + j] = count;
      count++;
    }
  }

  for(int i = 0; i < rows; i++) {
    for(int j = 0; j < cols; j++) {
      empty_matrix[i*cols + j] = 0;
    }
  }

  //printf("\nINPUT MATRIX:\n");
  //print_matrices(h_in_matrix, rows, cols);

  size_of_matrix = cols * rows * sizeof(int);
  err = cudaMalloc(&d_in_matrix, size_of_matrix);
  printf("\nHost to device copy input matrix: %s\n",cudaGetErrorString(err));
  cudaMemcpy(d_in_matrix, h_in_matrix, size_of_matrix, cudaMemcpyHostToDevice);

  size_of_matrix = cols * rows * sizeof(int);
  err = cudaMalloc(&d_out_matrix, size_of_matrix);
  printf("Host to Device copy output matrix: %s\n",cudaGetErrorString(err));
  cudaMemcpy(d_out_matrix, empty_matrix, size_of_matrix, cudaMemcpyHostToDevice);

  dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
  dim3 dimGrid(int(ceil(float(cols)/float(dimBlock.x))), int(ceil(float(rows)/float(dimBlock.y))));

  cudaEventRecord(start, 0);
  //find_matrix_transpose<<<dimGrid, dimBlock>>>(d_in_matrix, d_out_matrix, rows, cols);
  //transposeCoalesced<<<dimGrid, dimBlock>>>(d_in_matrix, d_out_matrix, rows, cols);
  //if (mode == 1) {
  	get_transpose<<<dimGrid, dimBlock>>>(d_in_matrix, d_out_matrix, rows, cols);
  //} else {
  	//serial_get_transpose<<<1, 1>>>(d_in_matrix, d_out_matrix, rows, cols);
  	//serial_get_transpose(h_in_matrix, h_out_matrix, rows, cols);
  //}
  cudaEventRecord(stop, 0);

  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&elapsed_time, start, stop);

  err = cudaThreadSynchronize();
  printf("Kernel Function Execution: %s\n", cudaGetErrorString(err));

  size_of_matrix = cols * rows * sizeof(int);
  err = cudaMemcpy(h_out_matrix, d_out_matrix, size_of_matrix, cudaMemcpyDeviceToHost);
  printf("Device to Host Copy output matrix: %s\n",cudaGetErrorString(err));

  // printf("\nTRANSPOSE MATRIX:\n");
  // print_matrices(h_out_matrix, cols, rows);
  //if (mode == 1) {
    printf("\nPARALLEL VERSION - Time required by matrix transpose is: %f ms\n", elapsed_time);
  //} else {
  	//printf("\nSERIAL VERSION - Time required by matrix transpose is: %f ms\n", elapsed_time);
  //}

  cudaEventDestroy(start);
  cudaEventDestroy(stop);

  free(h_in_matrix);
  free(h_out_matrix);

  cudaFree(d_in_matrix);
  cudaFree(d_out_matrix);


}